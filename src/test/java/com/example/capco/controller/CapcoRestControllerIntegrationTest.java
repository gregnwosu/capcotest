/*
 * Copyright (c) 2021. Greg Nwosu - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the AGPL license. https://www.gnu.org/licenses/agpl-3.0.html
 *
 */

package com.example.capco.controller;

import com.example.capco.controller.CapcoRestController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;


@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "build/snippets")
public class CapcoRestControllerIntegrationTest {

    @Autowired
    CapcoRestController controller;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test_shouldValidateTrade() throws Exception {
        MediaType textPlainUtf8 = new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8);
        String tradeJson = "{\"productType\":\"OPTION\", \"price\":3.5, \"settlementCurrency\":\"CHF\", \"transactionCurrency\":\"USD\", \"premiumDate\":\"2020-06-02T08:26:21.000Z\",\"deliveryDate\":\"2022-06-02T08:26:21.000Z\",\"valueDate\":\"2020-06-02T08:26:21.000Z\" ,\"exerciseStartDate\":\"2016-06-02T08:26:21.000Z\" , \"tradeDate\":\"2015-08-20T08:26:21.000Z\", \"counterParty\":\"YODA2\", \"volume\":40, \"expiryDate\":\"2017-08-20T08:26:21.000Z\" }";
        mockMvc.perform(MockMvcRequestBuilders.get("/trade/valid")
                .content(tradeJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(textPlainUtf8));
    }

    @Test
    public void test_shouldInValidateTradeBecauseOfCurrencyHoliday() throws Exception {
        MediaType textPlainUtf8 = new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8);
        String tradeJson = "{\"productType\":\"OPTION\", \"price\":3.5, \"settlementCurrency\":\"CHF\", \"transactionCurrency\":\"USD\", \"premiumDate\":\"2020-06-02T08:26:21.000Z\",\"deliveryDate\":\"2022-06-02T08:26:21.000Z\",\"valueDate\":\"2020-06-01T08:26:21.000Z\" ,\"exerciseStartDate\":\"2016-06-02T08:26:21.000Z\" , \"tradeDate\":\"2015-08-20T08:26:21.000Z\", \"counterParty\":\"YODA2\", \"volume\":40, \"expiryDate\":\"2017-08-20T08:26:21.000Z\" }";
        mockMvc.perform(MockMvcRequestBuilders.get("/trade/valid")
                .content(tradeJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentType(textPlainUtf8));
    }

    @Test
    public void shouldDisplayMetrics() throws Exception {
        MediaType textPlainUtf8 = new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8);
        mockMvc.perform(MockMvcRequestBuilders.get("/actuator/wavefront")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

}