/*
 * Copyright (c) 2021. Greg Nwosu - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the AGPL license. https://www.gnu.org/licenses/agpl-3.0.html
 *
 */

package com.example.capco;

import org.junit.Test;

import java.time.Instant;
import java.util.Currency;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class RulesTest {
    @Test
    public void test_AMERICANOPTION_EXPIRY_AND_PREMIUM_BEFORE_DELIVERY(){
        var trade = new Trade();
        trade.setProductType(ProductType.OPTION);
        trade.setExpiryDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setPremiumDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setDeliveryDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        assertFalse( Rules.AMERICANOPTION_EXPIRY_AND_PREMIUM_BEFORE_DELIVERY.test.test(trade));
        trade.setDeliveryDate(Instant.parse("2021-09-21T00:00:00.000Z"));
        assertTrue( Rules.AMERICANOPTION_EXPIRY_AND_PREMIUM_BEFORE_DELIVERY.test.test(trade));
    }

    @Test
    public void test_VALUE_DATE_NOT_ON_NEW_YEARS_DAY(){
        var trade = new Trade();
        trade.setProductType(ProductType.OPTION);
        trade.setExpiryDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setPremiumDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setValueDate(Instant.parse("2020-01-01T00:00:00.000Z"));
        assertFalse( Rules.VALUE_DATE_NOT_ON_NEW_YEARS_DAY.test.test(trade));
        trade.setValueDate(Instant.parse("2021-01-21T00:00:00.000Z"));
        assertTrue( Rules.VALUE_DATE_NOT_ON_NEW_YEARS_DAY.test.test(trade));
    }

    @Test
    public void test_VALUE_DATE_NOT_ON_WEEKEND(){
        var trade = new Trade();
        trade.setProductType(ProductType.OPTION);
        trade.setExpiryDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setPremiumDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setValueDate(Instant.parse("2020-09-05T00:00:00.000Z"));
        assertFalse( Rules.VALUE_DATE_NOT_ON_WEEKEND.test.test(trade));
        trade.setValueDate(Instant.parse("2021-09-08T00:00:00.000Z"));
        assertTrue( Rules.VALUE_DATE_NOT_ON_WEEKEND.test.test(trade));
    }

    @Test
    public void test_VALUE_DATE_DOES_NOT_FALL_ON_CURRENCY_HOLIDAY(){
        var trade = new Trade();
        trade.setProductType(ProductType.OPTION);
        trade.setSettlementCurrency(Currency.getInstance("CHF"));
        trade.setPremiumDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setValueDate(Instant.parse("2020-06-01T00:00:00.000Z"));
        assertFalse( Rules.VALUE_DATE_DOES_NOT_FALL_ON_CURRENCY_HOLIDAY.test.test(trade));
        trade.setValueDate(Instant.parse("2020-09-08T00:00:00.000Z"));
        assertTrue( Rules.VALUE_DATE_DOES_NOT_FALL_ON_CURRENCY_HOLIDAY.test.test(trade));
    }


    @Test
    public void test_SPOT_VALUE_DATE_SAME_AS_TRADE_DATE(){
        var trade = new Trade();
        trade.setProductType(ProductType.SPOT);
        trade.setSettlementCurrency(Currency.getInstance("USD"));
        trade.setTradeDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setValueDate(Instant.parse("2020-09-01T00:00:00.000Z"));
        assertFalse( Rules.SPOT_VALUE_DATE_SAME_AS_TRADE_DATE.test.test(trade));
        trade.setValueDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        assertTrue( Rules.SPOT_VALUE_DATE_SAME_AS_TRADE_DATE.test.test(trade));
    }

    @Test
    public void test_AMERICAN_OPTION_EXERCISE_START_DATE_BETWEEN_TRADE_AND_EXPIRY_DATE(){
        var trade = new Trade();
        trade.setProductType(ProductType.OPTION);
        trade.setExpiryDate(Instant.parse("2020-09-23T00:00:00.000Z"));
        trade.setTradeDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setExerciseStartDate(Optional.empty());
        assertFalse( Rules.AMERICAN_OPTION_EXERCISE_START_DATE_BETWEEN_TRADE_AND_EXPIRY_DATE.test.test(trade));
         trade.setExerciseStartDate(Optional.of(Instant.parse("2020-09-20T00:00:00.000Z")));
        assertFalse( Rules.AMERICAN_OPTION_EXERCISE_START_DATE_BETWEEN_TRADE_AND_EXPIRY_DATE.test.test(trade));
        trade.setExerciseStartDate(Optional.of(Instant.parse("2020-09-22T00:00:00.000Z")));
        assertTrue( Rules.AMERICAN_OPTION_EXERCISE_START_DATE_BETWEEN_TRADE_AND_EXPIRY_DATE.test.test(trade));
    }

    @Test
    public void test_FORWARD_VALUE_DATE_IS_AFTER_TRADE_DATE_BY_ONE_DAY(){
        var trade = new Trade();
        trade.setProductType(ProductType.FORWARD);
        trade.setValueDate(Instant.parse("2020-09-21T00:00:00.000Z"));
        trade.setTradeDate(Instant.parse("2020-09-23T00:00:00.000Z"));

        assertFalse( Rules.FORWARD_VALUE_DATE_IS_AFTER_TRADE_DATE_BY_AT_LEAST_ONE_DAY.test.test(trade));
        trade.setValueDate(Instant.parse("2020-09-24T00:00:00.000Z"));
        assertTrue( Rules.FORWARD_VALUE_DATE_IS_AFTER_TRADE_DATE_BY_AT_LEAST_ONE_DAY.test.test(trade));
    }
}