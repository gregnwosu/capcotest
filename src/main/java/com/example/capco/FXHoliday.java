package com.example.capco;

import lombok.AllArgsConstructor;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

@AllArgsConstructor
public class FXHoliday {
    public static final String EASTER_MONDAY = "Easter Monday";
    public static final String CHINESE_NEW_YEAR_HOLIDAY = "Chinese New Year Holiday";
    public static final String LABOUR_DAY = "Labour Day";
    public static final String CHINESE_NATIONAL_DAY_HOLIDAY = "Chinese National Day Holiday";
    public static final String CHRISTMAS_DAY = "Christmas Day";
    public static final String GOOD_FRIDAY = "Good Friday";
    public static final String LABOUR_DAY_HOLIDAY = "Labour Day Holiday)";
    public static final String EASTER_MONDAY_HOLIDAY = "Easter Monday Holiday)";
    public static final String ASCENSION_DAY = "Ascension Day";
    public static final String QUEEN_S_BIRTHDAY_HOLIDAY = "Queen’s Birthday Holiday)";
    public static final String CORPUS_CHRISTI_HOLIDAY = "Corpus Christi Holiday)";
    public static final String ALL_SAINTS_DAY = "All Saints’ Day";
    public static final String ST_STEPHEN_S_DAY_HOLIDAY = "St. Stephen’s Day Holiday)";
    static final List<FXHoliday> holidays = List.of(

            new FXHoliday(LocalDate.of(2020, 1, 6), "Three King’s Day Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 1, 6), "Three King’s Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 1, 6), "Epiphany", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 1, 6), "La Befana", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 1, 13), "Coming-of-age Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 1, 17), "Lee-Jackson Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 1, 19), "Confederate Heroes’ Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 1, 20), "Wellington Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 1, 20), "Civil Rights Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 1, 20), "Martin Luther King Jr. Day", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 1, 20), "Robert E. Lee’s Birthday (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 1, 22), "Saint Vincent the Martyr Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 1, 24), CHINESE_NEW_YEAR_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 1, 25), "Chinese New Year", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 1, 26), CHINESE_NEW_YEAR_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 1, 27), "Australia Day (in lieu)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 1, 27), CHINESE_NEW_YEAR_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 1, 27), "Auckland Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 1, 28), CHINESE_NEW_YEAR_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 1, 29), CHINESE_NEW_YEAR_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 1, 30), CHINESE_NEW_YEAR_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 2, 3), "Nelson Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 2, 6), "Waitangi Day", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 2, 11), "National Foundation Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 2, 12), "Lincoln’s Birthday (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 2, 17), "Family Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 2, 17), "Louis Riel Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 2, 17), "Nova Scotia Heritage Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 2, 17), "President’s Day Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 2, 23), "The Emperor’s Birthday", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 2, 24), "The Emperor’s Birthday Holiday", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 2, 25), "Mardi Gras (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 2, 28), "Andalucía Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 3, 1), "Republic Day Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 3, 1), "Balearic Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 3, 2), LABOUR_DAY_HOLIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 3, 2), "Texas Independence Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 3, 3), "Town Meeting Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 3, 8), "International Women’s Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 3, 9), "Adelaide Cup Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 3, 9), "Canberra Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 3, 9), LABOUR_DAY_HOLIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 3, 9), "Taranaki Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 3, 16), "St. Patrick’s Day (Government Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 3, 17), "St. Patrick’s Day Holiday)", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 3, 19), "St. Joseph’s Day Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 3, 19), "St. Joseph’s Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 3, 20), "Vernal Equinox Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 3, 23), "Otago Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 3, 26), "Prince Kuhio Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 3, 30), "Seward’s Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 3, 31), "Cesar Chavez Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 2), "Näfelser Fahrt Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 4, 4), "Ching Ming Festival", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 4, 5), "Ching Ming Festival Holiday", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 4, 6), "Ching Ming Festival Holiday", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 4, 9), "Holy Thursday Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 10), GOOD_FRIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 4, 10), "Good Friday Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 4, 10), GOOD_FRIDAY, Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 4, 10), GOOD_FRIDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 10), GOOD_FRIDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 10), "Good Friday Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 10), GOOD_FRIDAY, Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 4, 10), GOOD_FRIDAY, Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 4, 10), "Good Friday (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 10), "State Holiday (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 11), "Holy Saturday Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 4, 12), "Easter Sunday Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY_HOLIDAY, Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY_HOLIDAY, Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY_HOLIDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY_HOLIDAY, Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 13), EASTER_MONDAY, Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 4, 14), "Easter Tuesday (Government Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 4, 14), "Southland Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 4, 16), "Emancipation Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 20), "St. George’s Day (Government Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 4, 20), "Sechselauten Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 4, 20), "Feast of San Vincent Ferrer Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 20), "Patriots Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 21), "San Jacinto Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 23), "Castile and León Community Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 23), "St. George’s Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 24), "Arbor Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 25), "Anzac Day", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 4, 25), "Feast of St. Mark Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 25), "Liberation Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 27), "Anzac Day (in lieu) Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 4, 27), "Anzac Day", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 4, 27), "Confederate Memorial Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 4, 28), "Sardinia Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 4, 29), "Showa Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 5, 1), "May Day Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 5, 1), LABOUR_DAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 5, 1), LABOUR_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 1), LABOUR_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 1), LABOUR_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 1), LABOUR_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 2), "Community Festival of Madrid Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 3), "Constitution Memorial Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 5, 4), LABOUR_DAY_HOLIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 5, 4), "Labour Day Holiday", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 5, 4), "Greenery Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 5, 5), "Labour Day Holiday", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 5, 5), "Children’s Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 5, 5), "Primary Election Day Indiana (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 5, 6), "Constitution Memorial Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 5, 8), "Victory in Europe Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 8), "V-E Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 8), "Early May Bank Holiday", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 5, 8), "Truman Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 5, 11), "Confederate Memorial Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 5, 12), "Primary Election Day West Virginia (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 5, 13), "Saint Peter de Regalado Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 15), "Feast of St. Isidro Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 17), "Galician Literature Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 18), "National Patriots’ Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 5, 18), "Victoria Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 5, 21), ASCENSION_DAY, Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 5, 21), ASCENSION_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 21), ASCENSION_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 25), "Spring Bank Holiday", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 5, 25), "Memorial Day", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 5, 30), "Canary Islands Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 5, 31), "Castilla-La Mancha Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Reconciliation Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Western Australia Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Whit Monday Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Whit Monday", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Whit Monday Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Whit Monday", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Queen’s Birthday", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 6, 1), "Jefferson Davis’ Birthday (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 6, 2), "Republic Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 8), QUEEN_S_BIRTHDAY_HOLIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 6, 9), "Day of Murcia Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 9), "La Rioja Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 11), CORPUS_CHRISTI_HOLIDAY, Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 6, 11), CORPUS_CHRISTI_HOLIDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 11), CORPUS_CHRISTI_HOLIDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 11), "King Kamehameha I Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 6, 13), "Feast of St. Anthony Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 19), "Emancipation Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 6, 20), "West Virginia Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 6, 21), "National Aboriginal Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 6, 22), "Discovery Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 6, 23), "Fête d’Indépendance Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 6, 24), "The National Holiday of Quebec Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 6, 24), "Feast of St. John the Baptist Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 24), "Feast of St. John the Baptist Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 6, 25), "Dragon Boat Festival", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 6, 26), "Dragon Boat Festival Holiday", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 6, 29), "Saint Peter and Saint Paul Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 6, 29), "Saint Peter and Saint Paul Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 7, 1), "Canada Day", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 7, 3), "Independence Day (in lieu) Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 7, 4), "Independence Day", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 7, 6), "Independence Day Holiday (in lieu) Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 7, 9), "Nunavut Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 7, 13), "Orangeman’s Day (Government Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 7, 13), "Battle of the Boyne (in lieu) Holiday)", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 7, 14), "Bastille Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 7, 15), "Feast of St. Rosalia Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 7, 23), "Marine Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 7, 24), "Health-Sports Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 7, 24), "Pioneer Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 7, 25), "St. James’ Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 7, 28), "Day of Cantabria Institutions Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 7, 31), "Eid El Kebir Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 8, 1), "National Day", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 8, 3), "Picnic Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 8, 3), "Civic Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 8, 3), "Heritage Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 8, 3), "Summer bank holiday Holiday)", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 8, 5), "Our Lady of Africa Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 8, 10), "Mountain Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 8, 10), "Victory Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 8, 12), "Ekka People’s Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 8, 15), "Assumption Day Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 8, 15), "Assumption Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 8, 15), "Assumption Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 8, 15), "Assumption Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 8, 15), "Ferragosto", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 8, 17), "Discovery Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 8, 17), "Bennington Battle Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 8, 21), "Statehood Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 8, 27), "Lyndon Baines Johnson Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 8, 31), "August Bank Holiday Holiday)", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 9, 2), "Ceuta Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 7), LABOUR_DAY, Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 9, 7), "Labor Day", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 9, 8), "Feast of Our Lady of Victories Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 10), "Jeûne genevois Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 9, 11), "National Day of Catalonia Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 14), "Knabenschiessen Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 9, 15), "Lady of Aparecida Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 17), "Day of Melilla Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 19), "Feast of St. Gennaro Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 20), "Swiss Federal Fast Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 9, 20), "World Children’s Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 21), "Bettagsmontag Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 9, 21), "Respect for the Aged Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 9, 22), "Autumnal Equinox Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 9, 24), "La Mercè Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 9, 25), "Saint Nicholas of Flüe Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 9, 28), QUEEN_S_BIRTHDAY_HOLIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 9, 28), "South Canterbury Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 10, 1), "Chinese National Day", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 1), "Mid Autumn Festival", Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 2), "Grand Final Eve Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 10, 2), CHINESE_NATIONAL_DAY_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 3), CHINESE_NATIONAL_DAY_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 3), "German Unity Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 10, 4), CHINESE_NATIONAL_DAY_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 4), "Feast of St. Petronius Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 10, 5), LABOUR_DAY_HOLIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 10, 5), QUEEN_S_BIRTHDAY_HOLIDAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 10, 5), CHINESE_NATIONAL_DAY_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 6), CHINESE_NATIONAL_DAY_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 7), CHINESE_NATIONAL_DAY_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 8), CHINESE_NATIONAL_DAY_HOLIDAY, Currency.getInstance("CNY")),
            new FXHoliday(LocalDate.of(2020, 10, 9), "Valencian Community Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 10, 12), "Thanksgiving Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 10, 12), "Hispanic Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 10, 12), "Columbus Day Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 10, 12), "US Indigenous People’s Day Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 10, 19), "Alaska Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 10, 23), "Hawke’s Bay Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 10, 26), LABOUR_DAY, Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 10, 30), "Nevada Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 10, 31), "Reformation Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 1), "All Saints’ Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 1), ALL_SAINTS_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 1), ALL_SAINTS_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 1), ALL_SAINTS_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 2), "All Saints’ Day (in lieu) Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 2), "Feast of St. Giusto Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 2), "Marlborough Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 11, 3), "Melbourne Cup Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 11, 3), "Culture Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 11, 3), "Election Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 11, 5), "Return Day (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 11, 9), "Our Lady of the Almudena Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 11), "Remembrance Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 11, 11), "Armistice Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 11), "Veterans Day Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 11, 13), "Christchurch Show Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 11, 18), "Repentance Day Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 11, 23), "Labour Thanksgiving Day", Currency.getInstance("JPY")),
            new FXHoliday(LocalDate.of(2020, 11, 26), "Thanksgiving", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 11, 27), "Day after Thanksgiving Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 11, 27), "Lincoln’s Birthday (observed) (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 11, 30), "St. Andrew’s Day Holiday)", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 11, 30), "Chatham Islands Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 11, 30), "Westland Anniversary Day Holiday)", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 12, 3), "Feast of St. Francis Xavier Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 6), "Constitution Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 6), "Feast of St. Nicholas Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 7), "Constitution Day (in lieu) Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 7), "Feast of St. Ambrose Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 8), "Immaculate Conception Day Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 12, 8), "Immaculate Conception Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 8), "Immaculate Conception Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 24), "Christmas Eve Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 12, 24), "Christmas Eve (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 12, 24), "Washington’s Birthday Holiday (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 12, 25), CHRISTMAS_DAY, Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "Boxing Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "Boxing Day Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 12, 26), ST_STEPHEN_S_DAY_HOLIDAY, Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "St. Stephen’s Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "Christmas Holiday Holiday)", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 26), ST_STEPHEN_S_DAY_HOLIDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 26), ST_STEPHEN_S_DAY_HOLIDAY, Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "St. Stephen’s Day", Currency.getInstance("EUR")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "Boxing Day", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "Boxing Day Holiday", Currency.getInstance("NZD")),
            new FXHoliday(LocalDate.of(2020, 12, 26), "Day after Christmas (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 12, 28), "Boxing Day Holiday Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 12, 28), "Proclamation Day Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 12, 28), "Boxing Day (Government Holiday)", Currency.getInstance("CAD")),
            new FXHoliday(LocalDate.of(2020, 12, 28), "Boxing Day (in lieu)", Currency.getInstance("GBP")),
            new FXHoliday(LocalDate.of(2020, 12, 28), "Day after Christmas (Government Holiday)", Currency.getInstance("USD")),
            new FXHoliday(LocalDate.of(2020, 12, 31), "New Year’s Eve Holiday)", Currency.getInstance("AUD")),
            new FXHoliday(LocalDate.of(2020, 12, 31), "Restoration Day Holiday)", Currency.getInstance("CHF")),
            new FXHoliday(LocalDate.of(2020, 12, 31), "New Year’s Eve (Government Holiday)", Currency.getInstance("USD"))
    );
    LocalDate date;
    String description;
    Currency currency;
}
