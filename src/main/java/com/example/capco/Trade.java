package com.example.capco;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.Optional;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ValidateTrade(message = "value date cannot be before trade date", ruleName = "VALUE_DATE_NOT_BEFORE_TRADE")
@ValidateTrade(message = "value date cannot be on New Years Day", ruleName = "VALUE_DATE_NOT_ON_NEW_YEARS_DAY")
@ValidateTrade(message = "value date cannot be on weekend", ruleName = "VALUE_DATE_NOT_ON_WEEKEND")
@ValidateTrade(message = "value cannot fall on a currency holiday", ruleName = "VALUE_DATE_DOES_NOT_FALL_ON_CURRENCY_HOLIDAY")
@ValidateTrade(message = "spot trades must mature on the same day", ruleName = "SPOT_VALUE_DATE_SAME_AS_TRADE_DATE")
@ValidateTrade(message = "american options should have exercisestartdate between trade date and expiry date", ruleName = "AMERICAN_OPTION_EXERCISE_START_DATE_BETWEEN_TRADE_AND_EXPIRY_DATE")
@ValidateTrade(message = "american options should have expiry and premium dates before delivery", ruleName = "AMERICANOPTION_EXPIRY_AND_PREMIUM_BEFORE_DELIVERY")
@ValidateTrade(message = "forward trades must have at least 1 days difference between trade date and value date",
        ruleName = "FORWARD_VALUE_DATE_IS_AFTER_TRADE_DATE_BY_AT_LEAST_ONE_DAY")
public class Trade {
    @NotNull
    Currency transactionCurrency;
    @NotNull
    Currency settlementCurrency;
    @NotNull
    BigDecimal price;
    @NotNull
    Instant valueDate;
    @NotNull
    Instant tradeDate;
    @NotNull
    Instant expiryDate;
    @NotNull
    Instant premiumDate;
    @NotNull
    Instant deliveryDate;
    Optional<Instant> exerciseStartDate;
    @NotNull
    ProductType productType;
    @NotNull
    CounterParty counterParty;
    @NotNull
    @Min(1)
    int volume;
}


