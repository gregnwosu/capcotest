/* Copyright (c) $today.year. Greg Nwosu - All Rights Reserved
        You may use, distribute and modify this code under the
        terms of the AGPL license. https://www.gnu.org/licenses/agpl-3.0.html*/

package com.example.capco;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.function.Predicate;

enum Rules {
    VALUE_DATE_NOT_BEFORE_TRADE(Predicate.not(trade -> trade.valueDate.isBefore(trade.tradeDate))),
    VALUE_DATE_ON_SATURDAY(trade -> trade.valueDate.atOffset(ZoneOffset.UTC).getDayOfWeek() == DayOfWeek.SATURDAY),
    VALUE_DATE_ON_SUNDAY(trade -> trade.valueDate.atOffset(ZoneOffset.UTC).getDayOfWeek() == DayOfWeek.SUNDAY),
    VALUE_DATE_NOT_ON_WEEKEND(Predicate.not(VALUE_DATE_ON_SATURDAY.test.or(VALUE_DATE_ON_SUNDAY.test))),
    VALUE_DATE_NOT_ON_NEW_YEARS_DAY(Predicate.not(trade -> LocalDate.ofInstant(trade.valueDate, ZoneId.systemDefault()).compareTo(LocalDate.of(2020, 1, 1)) == 0)),
    SPOT_VALUE_DATE_SAME_AS_TRADE_DATE(trade -> trade.productType != ProductType.SPOT || trade.valueDate.equals(trade.tradeDate)),
    FORWARD_VALUE_DATE_IS_AFTER_TRADE_DATE_BY_AT_LEAST_ONE_DAY(trade -> trade.productType != ProductType.FORWARD || trade.valueDate.isAfter(trade.tradeDate.plus(1, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS).minus(1,ChronoUnit.NANOS))),
    AMERICAN_OPTION_EXERCISE_START_DATE_BETWEEN_TRADE_AND_EXPIRY_DATE(trade ->
            trade.productType != ProductType.OPTION ||
                    (trade.exerciseStartDate.isPresent()
                            && trade.exerciseStartDate.get().isAfter(trade.tradeDate)
                            && trade.exerciseStartDate.get().isBefore(trade.expiryDate))),
    AMERICANOPTION_EXPIRY_AND_PREMIUM_BEFORE_DELIVERY(trade -> trade.productType != ProductType.OPTION || (trade.expiryDate.isBefore(trade.deliveryDate) && trade.premiumDate.isBefore(trade.deliveryDate))),
    VALUE_DATE_DOES_NOT_FALL_ON_CURRENCY_HOLIDAY(trade -> FXHoliday.holidays.stream().filter(holiday -> holiday.date.compareTo(LocalDate.ofInstant(trade.valueDate, ZoneId.systemDefault())) == 0 && holiday.currency.equals(trade.settlementCurrency)).findFirst().isEmpty());
    final Predicate<Trade> test;

    Rules(Predicate<Trade> test) {
        this.test = test;
    }
}