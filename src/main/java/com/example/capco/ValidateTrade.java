package com.example.capco;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.function.Predicate;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = TradeCheckValidator.class)
@Documented
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Repeatable(ValidateTrade.List.class)
public @interface ValidateTrade {
    String message() default "validation error";

    String ruleName() default "alwaysTrue";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


    /**
     * Defines several <code>@TradeCheck</code> annotations on the same element
     *
     * @see ValidateTrade
     */
    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ValidateTrade[] value();
    }
}

class TradeCheckValidator implements ConstraintValidator<ValidateTrade, Trade> {
    Predicate<Trade> rule;

    public void initialize(ValidateTrade constraintAnnotation) {
        this.rule = Rules.valueOf(constraintAnnotation.ruleName()).test;
    }

    @Override
    public boolean isValid(Trade trade, ConstraintValidatorContext context) {
        try {
            return rule.test(trade);
        } catch (NullPointerException npe) {
            return false;
        }
    }
}
