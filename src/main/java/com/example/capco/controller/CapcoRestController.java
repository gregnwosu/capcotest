/*
 * Copyright (c) 2021. Greg Nwosu - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the AGPL license. https://www.gnu.org/licenses/agpl-3.0.html
 *
 */

package com.example.capco.controller;

import com.example.capco.Trade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
class CapcoRestController {
    @GetMapping("/trade/valid")
    @PostMapping("/trade/valid")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> validateTrade(@Valid @RequestBody Trade trade) {
        return ResponseEntity.ok(new StringBuilder("\nTrade is valid\n\t").append(trade.toString()).toString());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public String handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        List<String> errors = new ArrayList<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> {
            String errorMessage = error.getDefaultMessage();
            errors.add(new StringBuilder("\n").append(error.getField()).append(": ").append(errorMessage).toString());
        });
        ex.getBindingResult().getGlobalErrors().forEach(error -> {
            String errorMessage = error.getDefaultMessage();
            errors.add(new StringBuilder("\n").append(error.getObjectName()).append(": ").append(errorMessage).toString());
        });
        return errors.stream().reduce("", String::concat);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public String handleConstraintExceptions(
            ConstraintViolationException ex) {
        List<String> errors = new ArrayList<>();
        ex.getConstraintViolations().forEach(violation -> {
            var propertyPath = violation.getPropertyPath().toString();
            errors.add(new StringBuilder("\n").append(propertyPath).append(": ").append(violation.getMessage()).toString());
        });
        return errors.stream().reduce("", String::concat);
    }
}
